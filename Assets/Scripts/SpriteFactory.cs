﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpriteFactory : MonoBehaviour {

    public static SpriteFactory Instance { get { return instance; } }
    private static SpriteFactory instance;
    private SpriteFactory() { }

    private void OnEnable()
    {
        if (instance == null)
            instance = this;
        if (instance != this)
            Destroy(this);
    }

    private static List<Sprite> placeableObjectSprites;

	void Start () {
        placeableObjectSprites = new List<Sprite>();
        placeableObjectSprites = Resources.LoadAll<Sprite>("Sprites/PlaceableObjects").ToList();
	}
	
    public static Sprite GetPlaceableObjecSpriteByName(string spriteName)
    {
        if (placeableObjectSprites.Find((x) => x.name == spriteName))
        {
            return placeableObjectSprites.Find((x) => x.name == spriteName);
        }
        else
        {
            Debug.LogError("No sprite named " + spriteName);
            return null;
        }
    }
}
