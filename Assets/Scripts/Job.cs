﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Job {

    public Tile Tile { get; private set; }
    public ToolType RequiredTool { get; private set; }

    public string jobObjectType;

    private float timeToComplete;
    private Action<Job> cbJobCompleted;
    private Action<Job> cbJobCanceled;

    public Job(Tile tile, string jobObjectType, Action<Job> cbJobCompleted, ToolType requiredTool = ToolType.None, float jobTime = 1f)
    {
        this.jobObjectType = jobObjectType;
        this.Tile = tile;
        this.cbJobCompleted += cbJobCompleted;
        this.timeToComplete = jobTime;
        this.RequiredTool = requiredTool;
    }

    public void DoWork()
    {
        timeToComplete -= Time.deltaTime;

        if(timeToComplete <= 0)
        {
            cbJobCompleted?.Invoke(this);
        }
    }

    public void CancelJob()
    {
        cbJobCanceled?.Invoke(this);
    }

    public void RegisterJobCompleteCallback(Action<Job> cb)
    {
        cbJobCompleted += cb;
    }

    public void UnregisterJobCompleteCallback(Action<Job> cb)
    {
        cbJobCompleted -= cb;
    }

    public void RegisterJobCanceledCallback(Action<Job> cb)
    {
        cbJobCanceled += cb;
    }

    public void UnregisterJobCanceledCallback(Action<Job> cb)
    {
        cbJobCanceled -= cb;
    }
}
