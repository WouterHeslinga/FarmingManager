﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionManager : MonoBehaviour {

    public GameObject selectionIndicator;
    public GameObject selectedObject;
    public SelectionUI selectionUI;

    public IUnit selectedUnit;

    public void SelectUnit(GameObject unitGameObject)
    {
        selectedUnit?.RemoveStatusUpdateCallback();
        selectionUI.ResetUI();
        RemoveIndicator();
        if (!unitGameObject)
        {
            selectionUI.ResetUI();
            return;
        }

        if (unitGameObject.GetComponent<FarmLand>())
        {
            selectedUnit = unitGameObject.GetComponent<FarmLand>().farm;
        }
        else
        {
            selectedUnit = unitGameObject.GetComponent<IUnit>();
        }

        if (selectedUnit == null)
            return;

        selectedObject = unitGameObject;
        SetIndicator();

        selectedUnit.SetStatusUpdateCallback(selectionUI.SetText);
        selectionUI.SetText(selectedUnit.StatusText);

        foreach(IUnitCommand command in selectedUnit.Commands)
        {
            selectionUI.AddButton(() => command.DoCommand(), command.CommandName);
        }
    }

    void SetIndicator()
    {
        selectionIndicator.SetActive(true);
        selectionIndicator.transform.SetParent(selectedObject.transform);
        selectionIndicator.transform.localPosition = Vector3.zero;
        selectionIndicator.transform.localScale = Vector3.one;
    }

    void RemoveIndicator()
    {
        selectionIndicator.SetActive(false);
        selectionIndicator.transform.SetParent(null);
    }
}
