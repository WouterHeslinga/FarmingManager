﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolManager : MonoBehaviour
{
    public static ToolManager Instance { get { return instance; } }
    private static ToolManager instance;

    private List<Tool> tools;
    private List<Garage> garages;

    private void OnEnable()
    {
        if (instance == null)
            instance = this;
        if (instance != this)
            Destroy(this);

        tools = new List<Tool>();
        garages = new List<Garage>();
    }

    public Tool GetClosestTool(Vector3 location, ToolType toolType)
    {
        var distance = Mathf.Infinity;
        Tool closestTool = null;
        foreach (var item in tools)
        {
            if (item.type == toolType)
            {
                if (Vector3.Distance(item.transform.position, location) < distance)
                {
                    distance = Vector3.Distance(item.transform.position, location);
                    closestTool = item;
                }
            }
        }
        tools.Remove(closestTool);
        return closestTool;
    }

    public Garage GetClosestGarage(Vector3 location)
    {
        var distance = Mathf.Infinity;
        Garage closestGarage = null;
        foreach (var item in garages)
        {
            if (Vector3.Distance(item.transform.position, location) < distance)
            {
                distance = Vector3.Distance(item.transform.position, location);
                closestGarage = item;
            }
        }
        return closestGarage;
    }

    public void AddTool(Tool tool)
    {
        tools.Add(tool);
    }

    public void RemoveTool(Tool tool)
    {
        tools.Remove(tool);
    }

    public void AddGarage(Garage garage)
    {
        garages.Add(garage);
    }

    public void RemoveGarage(Garage garage)
    {
        garages.Remove(garage);
    }
}
