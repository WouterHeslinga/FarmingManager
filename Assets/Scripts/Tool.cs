﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum ToolType { None, Plow, Cultivator }

public class Tool : MonoBehaviour, ITooltip {
    public Tile Tile;
    public ToolType type;
    public float weight;

    private void Start()
    {
        Tile = WorldController.GetTileAtWorldCoord(transform.position);
        ToolManager.Instance.AddTool(this);
    }

    public string GetTooltip()
    {
        return $"{type}, {weight}";
    }
}
