﻿using System;

public class BasicCommand : IUnitCommand
{
    private Action doCommand;
    private string commandName;

    public string CommandName
    {
        get { return commandName; }
    }

    public BasicCommand(string name, Action command)
    {
        commandName = name;
        doCommand = command;
    }

    public void DoCommand()
    {
        doCommand();
    }
}