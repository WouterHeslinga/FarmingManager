﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class World
{
    private Tile[,] tiles;

    public TileGraph tileGraph;

    public int Height { get; private set; }
    public int Width { get; private set; }

    Action<Tile> cbTileChanged;

    public World(int width, int height)
    {
        Width = width;
        Height = height;

        tiles = new Tile[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                tiles[x, y] = new Tile(this, x, y);
                tiles[x, y].RegisterTileTypeChanged(OnTileChanged);
            }
        }
        tileGraph = new TileGraph(this);
    }

    public Tile GetTileAt(int x, int y)
    {
        if (x >= Width || x < 0 || y < 0 || y >= Height)
        {
            return null;
        }
        else
        {
            return tiles[x, y];
        }
    }

    public void DestroyInstalledObject(Tile t)
    {
        if(t.installedObject != null)
        {
            t.installedObject.DestroyObject();
        }
    }

    public void InvalidateTileGraph()
    {
        tileGraph = new TileGraph(this);
    }

    public void RegisterTileChanged(Action<Tile> callback)
    {
        cbTileChanged += callback;
    }

    public void UnregisterTileChanged(Action<Tile> callback)
    {
        cbTileChanged -= callback;
    }

    void OnTileChanged(Tile t)
    {
        if (cbTileChanged == null)
            return;

        cbTileChanged(t);
        InvalidateTileGraph();
    }

    public bool IsInstalledObjectPlacementValid(Tile tile, int width, int height)
    {
        if(width > 1 || height > 1)
        {
            for (int x = (tile.X - width/2); x < (tile.X + width/2); x++)
            {
                for (int y = (tile.Y - height / 2); y < (tile.Y + height / 2); y++)
                {
                    Tile t = WorldController.GetTileAtWorldCoord(x, y);
                    if (t == null || t.installedObject != null || t.Type != TileType.Ground)
                        return false;
                }
            }
        }
        else
        {
            if (tile.installedObject != null || tile.Type != TileType.Ground)
                return false;
        }

        return true;
    }
}
