﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileType { Ground, Water, Empty }

public class Tile : ITooltip
{
    public int X { get; private set; }
    public int Y { get; private set; }
    public World World { get; private set; }

    public PlaceableObject installedObject;
    public Job pendingJob;

    private Action<Tile> cbTileChanged;

    public float MovementCost
    {
        get
        {
            if (type == TileType.Empty || type == TileType.Water)
                return 0;

            if (type == TileType.Ground && installedObject == null)
                return 1;

            return 1 * installedObject.movementMultiplier;
        }
    }

    private TileType type = TileType.Ground;
    public TileType Type
    {
        get { return type; }
        set { type = value; cbTileChanged?.Invoke(this); }
    }

    public Tile(World world, int x, int y)
    {
        World = world;
        X = x;
        Y = y;
    }

    public void RegisterTileTypeChanged(Action<Tile> action)
    {
        cbTileChanged += action;
    }

    public bool InstallObject(PlaceableObject objectInstance)
    {
        if (objectInstance == null)
        {
            installedObject = null;
            return true;
        }

        if (installedObject != null)
        {
            Debug.Log("Trying to assign an object to a tile that already has one");
            return false;
        }

        installedObject = objectInstance;
        return true;
    }

    public bool IsNeighbour(Tile tile, bool checkDiagonal)
    {
        return Mathf.Abs(tile.X - X) + Mathf.Abs(tile.Y - Y) == 1 ||
            (checkDiagonal && (Mathf.Abs(tile.X - X) == 1 && Mathf.Abs(tile.Y - Y) == 1));
    }

    public Tile[] GetNeighbours(bool checkDiagonal = false)
    {
        Tile[] neighbours = checkDiagonal ? new Tile[8] : new Tile[4];
        Tile tile;

        tile = World.GetTileAt(X, Y + 1);
        neighbours[0] = tile;

        tile = World.GetTileAt(X + 1, Y);
        neighbours[1] = tile;

        tile = World.GetTileAt(X, Y - 1);
        neighbours[2] = tile;

        tile = World.GetTileAt(X - 1, Y);
        neighbours[3] = tile;

        if (checkDiagonal == true)
        {
            tile = World.GetTileAt(X + 1, Y + 1);
            neighbours[4] = tile;

            tile = World.GetTileAt(X + 1, Y - 1);
            neighbours[5] = tile;

            tile = World.GetTileAt(X - 1, Y - 1);
            neighbours[6] = tile;

            tile = World.GetTileAt(X - 1, Y + 1);
            neighbours[7] = tile;
        }

        return neighbours;
    }

    public string GetTooltip()
    {
        return $"{X}-{Y}, {installedObject?.objectName}";
    }
}
