﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Vehicle : MonoBehaviour, IUnit
{
    public Tile CurrentTile { get; private set; }
    public bool ShouldWork { get; private set; }

    public Transform attachmentPlace;
    public float maxSpeed;

    private float currentSpeed = 5f;
    private Tile destTile;
    private Tile nextTile;
    private AStar pathfinding;
    private float movementPercentage;
    private Job currentJob;
    private Stack<Job> jobStack;
    private Tool attachedTool;

    private List<IUnitCommand> unitCommands;
    private Action<string> onStatusUpdated;

    public string StatusText
    {
        get { return $"{transform.name} \n" +
                $"Attached tool: {(attachedTool ? attachedTool.type : ToolType.None)} \n " +
                $"Status: {(ShouldWork ? "Working" : "Idle")} "; }
    }

    public List<IUnitCommand> Commands
    {
        get { return unitCommands; }
    }

    public void SetStatusUpdateCallback(Action<string> onStatusUpdated)
    {
        this.onStatusUpdated = onStatusUpdated;
    }

    public void RemoveStatusUpdateCallback()
    {
        onStatusUpdated = null;
    }

    void Start()
    {
        jobStack = new Stack<Job>();
        CurrentTile = destTile = WorldController.GetTileAtWorldCoord(transform.position);
        currentSpeed = maxSpeed;
        ShouldWork = true;

        BasicCommand command = new BasicCommand("Work", ToggleShouldWork);
        BasicCommand command1 = new BasicCommand("DetachTool", DetachTool);

        unitCommands = new List<IUnitCommand>
        {
            command,
            command1
        };
    }

    void Update()
    {
        if(ShouldWork)
            Work();
        Movement();
    }

    void Work()
    {
        if (currentJob == null)
        {
            currentJob = jobStack.Count > 0 ? currentJob = jobStack.Pop() : JobQueue.Instance.Count() > 0 ? currentJob = JobQueue.Instance.Dequeue() : currentJob = null;

            if (currentJob != null)
            {
                if (attachedTool && currentJob.RequiredTool != ToolType.None && currentJob.RequiredTool == attachedTool.type)
                {                    
                    destTile = currentJob.Tile;
                    currentJob.RegisterJobCanceledCallback(OnJobEnded);
                    currentJob.RegisterJobCompleteCallback(OnJobEnded);
                }
                else
                {
                    if (attachedTool)
                    {
                        jobStack.Push(currentJob);
                        //FIXME: There is a quick fix for if there is no garage, needs to be implemented properly
                        Garage nearestGarage = ToolManager.Instance.GetClosestGarage(transform.position);
                        destTile = nearestGarage ? nearestGarage.BaseTile : WorldController.GetTileAtWorldCoord(destTile.X, destTile.Y -5);
                        currentJob = new Job(destTile, "DropTool", (Job) =>
                        {
                            nearestGarage?.StoreTool(attachedTool);
                            DetachTool();
                        },ToolType.None, 2);

                        currentJob.RegisterJobCanceledCallback(OnJobEnded);
                        currentJob.RegisterJobCompleteCallback(OnJobEnded);
                    }
                    else
                    {
                        Tool toolToGet = ToolManager.Instance.GetClosestTool(transform.position, currentJob.RequiredTool);
                        destTile = toolToGet.Tile;
                        jobStack.Push(currentJob);

                        currentJob = new Job(destTile, "GetTool", (Job) => { AttachTool(toolToGet); }, ToolType.None, 0.25f);
                        currentJob.RegisterJobCanceledCallback(OnJobEnded);
                        currentJob.RegisterJobCompleteCallback(OnJobEnded);
                    }
                }
            }
        }

        if (currentJob != null && CurrentTile == currentJob.Tile)
        {
            currentJob.DoWork();
        }
    }

    void Movement()
    {
        if (CurrentTile == destTile)
        {
            pathfinding = null;
            return;
        }

        if (nextTile == null || nextTile == CurrentTile)
        {
            if (pathfinding == null || pathfinding.Length() == 0)
            {
                pathfinding = new AStar(CurrentTile.World, CurrentTile, destTile);
                if (pathfinding.Length() == 0)
                {
                    Debug.LogError("Path_AStar returned no path to destination!");
                    ClearJob(true);
                    pathfinding = null;
                    return;
                }
                nextTile = pathfinding.Dequeue();
            }

            nextTile = pathfinding.Dequeue();

            if (nextTile == CurrentTile)
            {
                Debug.LogError("Movement -- nextTile == currentTile??");
            }
        }

        float distToTravel = Mathf.Sqrt(
            Mathf.Pow(CurrentTile.X - nextTile.X, 2) +
            Mathf.Pow(CurrentTile.Y - nextTile.Y, 2)
        );

        if (nextTile.MovementCost == 0)
        {
            Debug.LogError("FIXME: A vehicle was trying to enter an unwalkable tile.");
            nextTile = null;
            pathfinding = null;
            return;
        }

        float distThisFrame = currentSpeed / nextTile.MovementCost * Time.deltaTime;

        float percThisFrame = distThisFrame / distToTravel;

        movementPercentage += percThisFrame;

        if (movementPercentage >= 1)
        {
            CurrentTile = nextTile;
            movementPercentage = 0;
        }

        var dir = new Vector3(nextTile.X, nextTile.Y,0) - transform.position;
        var angle = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) -90;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        transform.position = new Vector3(
            Mathf.Lerp(CurrentTile.X, nextTile.X, movementPercentage), 
            Mathf.Lerp(CurrentTile.Y, nextTile.Y, movementPercentage), 
            0
        );
    }

    void ClearJob(bool destroyJob)
    {
        nextTile = destTile = CurrentTile;
        pathfinding = null;

        if (destroyJob)
            JobQueue.Instance.DestroyJob(currentJob);
        else JobQueue.Instance.Enqueue(currentJob);

        currentJob = null;
    }

    void OnJobEnded(Job j)
    {
        if (j != currentJob)
        {
            Debug.LogError("Character being told about job that isn't his.");
            return;
        }
        currentJob = null;
    }

    void AttachTool(Tool toolToAttach)
    {
        toolToAttach.gameObject.SetActive(true);
        toolToAttach.transform.parent = attachmentPlace;
        attachedTool = toolToAttach;
        attachedTool.transform.rotation = new Quaternion(0, 0, 0, 0);
        attachedTool.transform.localPosition = new Vector3(0, 0, 0);
        currentSpeed = maxSpeed - (toolToAttach.weight / 200); 
    }

#region COMMANDS
    public void ToggleShouldWork()
    {
        ShouldWork = !ShouldWork;
        if(ShouldWork == false)
        {
            ClearJob(false);
        }

        onStatusUpdated?.Invoke(StatusText);
    }

    void DetachTool()
    {
        attachedTool.transform.parent = null;
        attachedTool = null;
        currentSpeed = maxSpeed;
    }

    //TODO: implement this.
    void MoveTo()
    {

    }
    #endregion
}
