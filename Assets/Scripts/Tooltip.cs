﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {
    static Text tooltipText;

    private void Start()
    {
        tooltipText = GetComponentInChildren<Text>();
    }

    public static void SetTooltip(string text)
    {
        tooltipText.text = text;
    }
}
