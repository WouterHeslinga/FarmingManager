﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseController : MonoBehaviour {

    //prototype
    public GameObject cursorIndicator;
    private SelectionManager selectionManager;

    private Vector3 lastFramePosition;
    private Vector3 dragStartPosition;
    private Vector3 currentFramePosition;
    private List<GameObject> dragPrototypes;
    private BuildController buildController;

    private RaycastHit2D hit;
    private Ray2D ray;

    void Start () {
        dragPrototypes = new List<GameObject>();
        buildController = FindObjectOfType<BuildController>();
        selectionManager = FindObjectOfType<SelectionManager>();
    }
	
	void Update()
    {
        currentFramePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        CameraMovement();
        if (buildController.buildModeObjectType != "")
            UpdateDragging(currentFramePosition);

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            selectionManager.SelectUnit(hit.transform?.gameObject);
        }

        Tile t = WorldController.GetTileAtWorldCoord(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        Tooltip.SetTooltip(t?.GetTooltip());

        lastFramePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        lastFramePosition.z = -10;
    }

    private void CameraMovement()
    {
        //Camera panning
        if (Input.GetMouseButton(2) || Input.GetMouseButton(1))
        {
            Vector3 diff = lastFramePosition - currentFramePosition;
            Camera.main.transform.Translate(diff);
        }

        Camera.main.orthographicSize -= Camera.main.orthographicSize * Input.GetAxisRaw("Mouse ScrollWheel");
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, 3f, 25f);
    }

    private void UpdateDragging(Vector3 currFramePosition)
    {
        //Check if mouse over interface
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        //Start drag
        if (Input.GetMouseButtonDown(0))
        {
            dragStartPosition = currFramePosition;
        }

        int startX = Mathf.FloorToInt(dragStartPosition.x + 0.5f);
        int endX = Mathf.FloorToInt(currFramePosition.x + 0.5f);
        int startY = Mathf.FloorToInt(dragStartPosition.y + 0.5f);
        int endY = Mathf.FloorToInt(currFramePosition.y + 0.5f);

        //Invert if needed
        if (endX < startX)
        {
            int temp = endX;
            endX = startX;
            startX = temp;
        }
        if (endY < startY)
        {
            int temp = endY;
            endY = startY;
            startY = temp;
        }

        foreach (var item in dragPrototypes)
        {
            SimplePool.Despawn(item);
        }

        dragPrototypes = new List<GameObject>();

        if (Input.GetMouseButton(0))
        {
            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y <= endY; y++)
                {
                    Tile t = WorldController.Instance.World.GetTileAt(x, y);
                    if (t != null)
                    {
                        GameObject dragProtoype = SimplePool.Spawn(cursorIndicator, new Vector3(x, y,0), Quaternion.identity);
                        dragProtoype.transform.parent = transform;
                        dragPrototypes.Add(dragProtoype);
                    }
                }
            }
        }

        //End drag
        if (Input.GetMouseButtonUp(0) && buildController.SelectedObject)
        {
            for (int x = startX; x <= endX; x++)
            {
                for (int y = startY; y <= endY; y++)
                {
                    Tile t = WorldController.Instance.World.GetTileAt(x, y);
                    if (t != null)
                    {
                        buildController.DoBuild(t);
                    }
                }
            }
        }
    }
}
