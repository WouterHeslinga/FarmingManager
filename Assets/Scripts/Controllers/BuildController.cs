﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildController : MonoBehaviour
{
    public GameObject buildingIndicatorPrefab;
    public string buildModeObjectType;
    public PlaceableObject SelectedObject { get; private set; }

    private bool buildModeIsObjects = false;
    private TileType buildType;
    private Vector3 currentMousePosition;
    private GameObject buildingIndicator;

    private void Update()
    {
        if (buildingIndicator)
        {
            currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Tile t = WorldController.GetTileAtWorldCoord(currentMousePosition);
            if (t != null)
            {
                buildingIndicator.transform.position = new Vector3(t.X, t.Y, 0);
                if (SelectedObject != null)
                    buildingIndicator.transform.localScale = new Vector3(SelectedObject.width, SelectedObject.height, 0);
                else
                    buildingIndicator.transform.localScale = new Vector3(1, 1, 0);

                if(buildModeObjectType == "destroy")
                {
                    buildingIndicator.GetComponent<SpriteRenderer>().color = Color.red;
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            buildModeObjectType = "";
            SelectedObject = null;
            Destroy(buildingIndicator);
            buildingIndicator = null;
            buildModeIsObjects = false;

        }
    }

    public void SetBuildMode(string type)
    {
        buildModeIsObjects = false;
        buildType = (TileType)Enum.Parse(typeof(TileType), type);
    }

    public void SetMode_BuildInstalledObject(string objectType)
    {
        buildModeIsObjects = true;
        buildModeObjectType = objectType;
        SelectedObject = PlaceableObjectFactory.Instance.GetPlaceableGameObject(buildModeObjectType)?.GetComponent<PlaceableObject>() ? PlaceableObjectFactory.Instance.GetPlaceableGameObject(buildModeObjectType)?.GetComponent<PlaceableObject>() : null;
        if (!buildingIndicator)
            buildingIndicator = Instantiate(buildingIndicatorPrefab);
    }

    public void DoBuild(Tile t)
    {
        if (buildModeObjectType == "destroy")
        {
            WorldController.Instance.World.DestroyInstalledObject(t);
            return;
        }

        if (buildModeIsObjects)
        {
            if (WorldController.Instance.World.IsInstalledObjectPlacementValid(t, SelectedObject.width, SelectedObject.height) && t.pendingJob == null)
                Instantiate(SelectedObject.gameObject, new Vector2(t.X, t.Y), Quaternion.identity, transform);

        }
        else
        {
            t.Type = buildType;
        }
    }
}
