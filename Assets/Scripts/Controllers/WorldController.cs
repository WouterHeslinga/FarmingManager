﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour {

    public static WorldController Instance { get; private set; }
    public World World { get; private set; }

	// Use this for initialization
	void OnEnable () {
        if (Instance == null)
            Instance = this;

        World = new World(100, 100);

        TileGraph tileGraph = new TileGraph(World);
	}

    public static Tile GetTileAtWorldCoord(Vector3 coord)
    {
        int x = Mathf.FloorToInt(coord.x + 0.5f);
        int y = Mathf.FloorToInt(coord.y + 0.5f);

        return Instance.World.GetTileAt(x, y); ;
    }

    public static Tile GetTileAtWorldCoord(int x, int y)
    {
        x = Mathf.FloorToInt(x + 0.5f);
        y = Mathf.FloorToInt(y + 0.5f);

        return Instance.World.GetTileAt(x, y); ;
    }
}
