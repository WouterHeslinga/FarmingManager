﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteTileController : MonoBehaviour {

    public Sprite groundSprite;
    public Sprite waterSprite;

    private Dictionary<Tile, GameObject> tileGameObjectMap;

    World World
    {
        get { return WorldController.Instance.World; }
    }

	void Start () {
        tileGameObjectMap = new Dictionary<Tile, GameObject>(); 

        for (int x = 0; x < World.Width; x++)
        {
            for (int y = 0; y < World.Height; y++)
            {
                GameObject tile_go = new GameObject($"Tile_{x}_{y}");
                tile_go.transform.SetParent(transform, true);
                Tile tile_data = World.GetTileAt(x, y);
                tile_go.transform.position = new Vector3(tile_data.X, tile_data.Y);

                tileGameObjectMap.Add(tile_data, tile_go);

                SpriteRenderer tileSr = tile_go.AddComponent<SpriteRenderer>();
                tileSr.sprite = groundSprite;
                tileSr.sortingLayerName = "Background";
            }
        }
        World.RegisterTileChanged(OnTileChanged);
	}

    void OnTileChanged(Tile tileData)
    {
        if (!tileGameObjectMap.ContainsKey(tileData))
        {
            Debug.LogError("tileGameObjectMap doesn't contain this key");
            return;
        }

        GameObject tileGo = tileGameObjectMap[tileData];

        if (!tileGo)
        {
            Debug.Log("tileGameObjectMap returned null");
            return;
        }

        if(tileData.Type == TileType.Ground)
        {
            tileGo.GetComponent<SpriteRenderer>().sprite = groundSprite;
        }
        else if(tileData.Type == TileType.Water)
        {
            tileGo.GetComponent<SpriteRenderer>().sprite = waterSprite;
        }
        else
        {
            tileGo.GetComponent<SpriteRenderer>().sprite = null;
        }
    }
}
