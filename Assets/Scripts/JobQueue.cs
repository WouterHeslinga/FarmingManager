﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JobQueue : MonoBehaviour {

    public static JobQueue Instance;

    private List<Job> jobQueue;
    private Action<Job> cbJobCreated;

    private void OnEnable()
    {
        if (Instance == null)
            Instance = this;
        if (Instance != this)
            Destroy(this);
    }

    private void Start()
    {
        jobQueue = new List<Job>();
    }

    public void Enqueue(Job j)
    {
        jobQueue.Add(j);
        cbJobCreated?.Invoke(j);
    }

    public int Count()
    {
        return jobQueue.Count;
    }

    public Job Dequeue()
    {
        if (jobQueue.Count == 0)
            return null;

        Job j = jobQueue[0];
        jobQueue.RemoveAt(0);
        return j;
    }

    public void DestroyJob(Job j)
    {
        if(jobQueue.Contains(j))
            jobQueue.Remove(j);
    }
}
