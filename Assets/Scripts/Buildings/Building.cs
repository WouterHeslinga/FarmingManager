﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : PlaceableObject {

    protected override void Start () {
        Tiles = new List<Tile>();
        cbOnChanged += SetSpriteBasedOnNeighbours;
        AssignBaseTiles();

        ChangeNeighbourTiles();
        if (linksToNeighbour)
            SetSpriteBasedOnNeighbours();
    }

    protected override void AssignBaseTiles()
    {
        if (width % 2 == 0 || height % 2 == 0)
            Debug.LogError("Building width or height isn't right");

        if (width > 1 || height > 1)
        {
            BaseTile = WorldController.GetTileAtWorldCoord(new Vector2(transform.position.x, transform.position.y));
            for (int x = (BaseTile.X - width / 2); x <= (BaseTile.X + (width / 2)); x++)
            {
                for (int y = (BaseTile.Y - height / 2); y <= (BaseTile.Y + (height / 2)); y++)
                {
                    Tiles.Add(WorldController.GetTileAtWorldCoord(x, y));
                    WorldController.GetTileAtWorldCoord(x, y).InstallObject(this);
                }
            }
        }
        else
        {
            BaseTile = WorldController.GetTileAtWorldCoord(transform.position);
            BaseTile.installedObject = this;
            Tiles.Add(BaseTile);
        }
    }
}
