﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garage : Building {

    private List<Tool> storedTools;

    protected override void Start()
    {
        base.Start();
        storedTools = new List<Tool>();
        ToolManager.Instance.AddGarage(this);
    }

    public void StoreTool(Tool tool)
    {
        storedTools.Add(tool);
        tool.Tile = BaseTile;
        tool.gameObject.transform.SetParent(transform);
        tool.gameObject.SetActive(false);
    }

    public Tool GetTool(ToolType toolType)
    {
        Tool temp = storedTools.Find(x => x.type == toolType);
        temp.gameObject.SetActive(true);
        temp.gameObject.transform.SetParent(null);
        return temp;
    }
}
