﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableObjectFactory : MonoBehaviour {

    public static PlaceableObjectFactory Instance { get { return instance; } }
    private static PlaceableObjectFactory instance;
    private PlaceableObjectFactory() { }

    private void OnEnable()
    {
        if (instance == null)
            instance = this;
        if (instance != this)
            Destroy(this);
    }

    public List<GameObject> placeableObjectPrefabs;

    public GameObject GetPlaceableGameObject(string objectName)
    {
        return placeableObjectPrefabs.Find((x) => x.name == objectName);
    }

    public PlaceableObject GetPlaceableObject(string objectName)
    {
        return placeableObjectPrefabs.Find(x => x.name == objectName).GetComponent<PlaceableObject>();
    }
}
