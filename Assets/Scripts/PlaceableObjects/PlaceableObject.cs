﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableObject : MonoBehaviour
{
    public string objectName;
    public bool linksToNeighbour;
    public float movementMultiplier;
    public int width, height;

    public Tile BaseTile;
    protected List<Tile> Tiles;
    protected Action cbOnChanged;

    protected virtual void Start()
    {
        Tiles = new List<Tile>();
        cbOnChanged += SetSpriteBasedOnNeighbours;

        AssignBaseTiles();        

        ChangeNeighbourTiles();
        if(linksToNeighbour)
            SetSpriteBasedOnNeighbours();
    }

    protected virtual void AssignBaseTiles()
    {
        if(width > 1 || height > 1)
        {
            BaseTile = WorldController.GetTileAtWorldCoord(new Vector3(transform.position.x - (width / 2), transform.position.y - (height / 2), 0));
            for (int x = BaseTile.X; x < (BaseTile.X + width); x++)
            {
                for (int y = BaseTile.Y; y < (BaseTile.Y + height); y++)
                {
                    Tiles.Add(WorldController.GetTileAtWorldCoord(x, y));
                    WorldController.GetTileAtWorldCoord(x, y).installedObject = this;
                }
            }
        }
        else
        {
            BaseTile = WorldController.GetTileAtWorldCoord(transform.position);
            BaseTile.installedObject = this;
            Tiles.Add(BaseTile);
        }
    }

    protected void SetSpriteBasedOnNeighbours()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        World world = WorldController.Instance.World;
        Tile ourTile = WorldController.GetTileAtWorldCoord(transform.position);
        Tile t;
        int x = ourTile.X;
        int y = ourTile.Y;

        //Set own sprite to right one
        if (linksToNeighbour == false)
        {
            sr.sprite = SpriteFactory.GetPlaceableObjecSpriteByName(objectName);
            return;
        }
        else
        {
            string spriteName = objectName + "_";

            t = world.GetTileAt(x, y + 1);
            if (t != null && t.installedObject && t.installedObject.linksToNeighbour)
                spriteName += "N";
            t = world.GetTileAt(x + 1, y);
            if (t != null && t.installedObject && t.installedObject.linksToNeighbour)
                spriteName += "E";
            t = world.GetTileAt(x, y - 1);
            if (t != null && t.installedObject && t.installedObject.linksToNeighbour)
                spriteName += "S";
            t = world.GetTileAt(x - 1, y);
            if (t != null && t.installedObject && t.installedObject.linksToNeighbour)
                spriteName += "W";

            sr.sprite = SpriteFactory.GetPlaceableObjecSpriteByName(spriteName);
        }        
    }

    protected void ChangeNeighbourTiles()
    {
        World world = WorldController.Instance.World;
        Tile t;

        foreach (Tile tile in Tiles)
        {
            int x = tile.X;
            int y = tile.Y;

            //North
            t = world.GetTileAt(x, y + 1);
            InvokeNeighbour(t);
            //East
            t = world.GetTileAt(x + 1, y);
            InvokeNeighbour(t);

            //South
            t = world.GetTileAt(x, y - 1);
            InvokeNeighbour(t);

            //West
            t = world.GetTileAt(x - 1, y);
            InvokeNeighbour(t);
        }
    }

    private void InvokeNeighbour(Tile t)
    {
        if (!Tiles.Contains(t))
            if (t.installedObject?.linksToNeighbour == true)
                t?.installedObject?.cbOnChanged?.Invoke();
    }

    public virtual void DestroyObject()
    {
        BaseTile.installedObject = null;
        ChangeNeighbourTiles();
        Destroy(gameObject);
    }
}
