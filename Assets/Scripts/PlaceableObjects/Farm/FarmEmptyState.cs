﻿using UnityEngine;

public class FarmEmptyState : IFarmState
{
    public string SpriteName { get { return "FarmLandPlowed"; } }
    public ToolType RequiredTool { get { return ToolType.Plow; } }

    public void Enter(Farm owner)
    {
    }

    public void Execute(Farm owner)
    {
    }

    public void Exit(Farm owner)
    {
        owner.StateMachine.ChangeState(new FarmPlowedState());
    }
}

