﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Farm : IUnit
{
    private List<IUnitCommand> unitCommands;
    public Action<string> OnStatusUpdated;

    public string StatusText
    {
        get { return $"{StateMachine.CurrentState} \n {WorkProgress}/{Size}"; }
    }

    public List<IUnitCommand> Commands
    {
        get { return unitCommands; }
    }

    public void SetStatusUpdateCallback(Action<string> onStatusUpdated)
    {
        OnStatusUpdated = onStatusUpdated;
    }

    public void RemoveStatusUpdateCallback()
    {
        OnStatusUpdated = null;
    }

    public Farm(FarmLand farmLand)
    {
        StateMachine = new FarmStateMachine(this, new FarmEmptyState());
        Farmlands = new List<FarmLand>
        {
            farmLand
        };

        BasicCommand command = new BasicCommand("Work", EnqueueJobs);

        unitCommands = new List<IUnitCommand>
        {
            command,
        };
    }

    public List<FarmLand> Farmlands { get; private set; }
    public int Size { get { return Farmlands.Count; } }
    public FarmStateMachine StateMachine { get; private set; }
    public int WorkProgress { get; private set; }

    public void AddFarmLand(FarmLand farmLandToAdd)
    {
        Farmlands.Add(farmLandToAdd);
    }

    public void AddFarm(Farm farmToAdd)
    {
        farmToAdd.Farmlands.ForEach((farmLand) =>
        {
            farmLand.farm = this;
        });

        Farmlands.AddRange(farmToAdd.Farmlands);
    }

    public void RemoveFarmLand(FarmLand farmLandToRemove)
    {
        Farmlands.Remove(farmLandToRemove);
    }

    public void EnqueueJobs()
    {
        Farmlands.ForEach(farmLand =>
        {
            JobQueue.Instance.Enqueue(new Job(farmLand.BaseTile, "work", (Job) =>
            {
                farmLand.SetNewSprite(StateMachine.CurrentState.SpriteName);
                UpdateProgress();
            }, StateMachine.CurrentState.RequiredTool));
        });
    }

    public void UpdateProgress()
    {
        WorkProgress++;
        CheckProgress();
        OnStatusUpdated?.Invoke(StatusText);
    }

    private void CheckProgress()
    {
        if (WorkProgress < Size)
            return;

        WorkProgress = 0;
        StateMachine.CurrentState.Exit(this);
    }
}
