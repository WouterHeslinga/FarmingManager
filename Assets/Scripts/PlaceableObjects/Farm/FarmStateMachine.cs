﻿using System.Collections;
using UnityEngine;

public class FarmStateMachine
{
    public FarmStateMachine(Farm owner, IFarmState initialState)
    {
        this.owner = owner;
        ChangeState(initialState);
    }

    public IFarmState CurrentState { get; private set; }
    private Farm owner;

    public void ChangeState(IFarmState newState)
    {
        Debug.Log($"StateMachine changed from {CurrentState} to {newState}");
        CurrentState = newState;
        CurrentState.Enter(owner);
    }
}
