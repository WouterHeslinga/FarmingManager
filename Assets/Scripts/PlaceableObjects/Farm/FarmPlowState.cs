﻿using UnityEngine;

public class FarmPlowedState : IFarmState
{
    public string SpriteName { get { return "FarmLandCultivated"; } }
    public ToolType RequiredTool { get { return ToolType.Cultivator; } }

    public void Enter(Farm owner)
    {
    }

    public void Execute(Farm owner)
    {
    }

    public void Exit(Farm owner)
    {
        owner.StateMachine.ChangeState(new FarmPlowedState());
    }
}

