﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class FarmLand : PlaceableObject {

    public Farm farm;

    protected override void Start()
    {
        base.Start();
        CheckForNearbyFarm();
        if(farm == null)
        {
            farm = new Farm(this);
        }
        JoinFarms();
    }

    void CheckForNearbyFarm()
    {
        World world = WorldController.Instance.World;
        Tile t;
        FarmLand farmland;

        //North
        t = world.GetTileAt(BaseTile.X, BaseTile.Y + 1);
        if(t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if(farm != null)
            {
                farmland.farm.AddFarm(farm);
                farm = farmland.farm;
                return;
            }
            else
            {
                farmland.farm.AddFarmLand(this);
                farm = farmland.farm;
                return;
            }
        }

        //East
        t = world.GetTileAt(BaseTile.X + 1, BaseTile.Y);
        if (t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if (farm != null)
            {
                farmland.farm.AddFarm(farm);
                farm = farmland.farm;
                return;
            }
            else
            {
                farmland.farm.AddFarmLand(this);
                farm = farmland.farm;
                return;
            }
        }

        //South
        t = world.GetTileAt(BaseTile.X, BaseTile.Y - 1);
        if (t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if (farm != null)
            {
                farmland.farm.AddFarm(farm);
                farm = farmland.farm;
                return;
            }
            else
            {
                farmland.farm.AddFarmLand(this);
                farm = farmland.farm;
                return;
            }
        }

        //West
        t = world.GetTileAt(BaseTile.X -1 , BaseTile.Y);
        if (t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if (farm != null)
            {
                farmland.farm.AddFarm(farm);
                farm = farmland.farm;
                return;
            }
            else
            {
                farmland.farm.AddFarmLand(this);
                farm = farmland.farm;
                return;
            }
        }
    }

    void JoinFarms()
    {
        World world = WorldController.Instance.World;
        Tile t;
        FarmLand farmland;

        //North
        t = world.GetTileAt(BaseTile.X, BaseTile.Y + 1);
        if (t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if (farmland.farm != farm)
            {
                farm.AddFarm(farmland.farm);
            }
        }

        //East
        t = world.GetTileAt(BaseTile.X + 1, BaseTile.Y);
        if (t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if (farmland.farm != farm)
            {
                farm.AddFarm(farmland.farm);
            }
        }

        //South
        t = world.GetTileAt(BaseTile.X, BaseTile.Y - 1);
        if (t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if (farmland.farm != farm)
            {
                farm.AddFarm(farmland.farm);
            }
        }

        //West
        t = world.GetTileAt(BaseTile.X - 1, BaseTile.Y);
        if (t?.installedObject?.GetType() == typeof(FarmLand))
        {
            farmland = t.installedObject as FarmLand;
            if (farmland.farm != farm)
            {
                farm.AddFarm(farmland.farm);
            }
        }
    }

    public void SetNewSprite(string spriteName)
    {
        if (!SpriteFactory.GetPlaceableObjecSpriteByName(spriteName + "_"))
            return;

        objectName = spriteName;
        SetSpriteBasedOnNeighbours();
    }
}
