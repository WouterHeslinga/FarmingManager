﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITooltip {
    string GetTooltip();
}

public interface IUnitCommand
{
    string CommandName { get; }
    void DoCommand();
}

public interface IUnit
{
    string StatusText { get; }
    List<IUnitCommand> Commands { get; }

    void SetStatusUpdateCallback(Action<string> onStatusUpdated);
    void RemoveStatusUpdateCallback();
}

public interface IFarmState
{
    string SpriteName { get; }
    ToolType RequiredTool { get; }

    void Enter(Farm owner);
    void Execute(Farm owner);
    void Exit(Farm owner);
}
