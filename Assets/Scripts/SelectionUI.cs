﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SelectionUI : MonoBehaviour
{
    private List<Button> selectedObjectCommands;

    public GameObject actionButtonPrefab;
    public Text statText;
    public Transform actionButtonPanel;

    private void Start()
    {
        selectedObjectCommands = new List<Button>();
    }

    public void AddButton(UnityAction func, string actionText)
    {
        Button newButton = Instantiate(actionButtonPrefab, actionButtonPanel).GetComponent<Button>();
        newButton.GetComponentInChildren<Text>().text = actionText;
        selectedObjectCommands.Add(newButton);

        newButton.onClick.AddListener(func);
    }

    public void SetText(string text)
    {
        statText.text = text;
    }

    public void ResetUI()
    {
        statText.text = "";
        foreach (Button button in selectedObjectCommands)
        {
            Destroy(button.gameObject);
        }
        selectedObjectCommands.Clear();
    }
}
