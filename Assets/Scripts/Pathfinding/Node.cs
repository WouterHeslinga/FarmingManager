﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node<T> {
    public T tile;
    public Edge<T>[] edges;
}
