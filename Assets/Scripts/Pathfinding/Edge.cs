﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge<T> {
    public float cost;
    public Node<T> node;
}
