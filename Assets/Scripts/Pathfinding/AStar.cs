﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;
using System.Linq;

public class AStar {
    private Queue<Tile> path;

    public AStar(World world, Tile tileStart, Tile tileEnd)
    {
        if (world.tileGraph == null)
        {
            world.tileGraph = new TileGraph(world);
        }

        Dictionary<Tile, Node<Tile>> nodes = world.tileGraph.Nodes;

        if (nodes.ContainsKey(tileStart) == false)
        {
            Debug.LogError("Path_AStar: The starting tile isn't in the list of nodes!");

            return;
        }
        if (nodes.ContainsKey(tileEnd) == false)
        {
            Debug.LogError("Path_AStar: The ending tile isn't in the list of nodes!");
            return;
        }

        Node<Tile> start = nodes[tileStart];
        Node<Tile> goal = nodes[tileEnd];
        List<Node<Tile>> ClosedSet = new List<Node<Tile>>();
        SimplePriorityQueue<Node<Tile>> OpenSet = new SimplePriorityQueue<Node<Tile>>();
        Dictionary<Node<Tile>, Node<Tile>> Came_From = new Dictionary<Node<Tile>, Node<Tile>>();
        Dictionary<Node<Tile>, float> g_score = new Dictionary<Node<Tile>, float>();
        Dictionary<Node<Tile>, float> f_score = new Dictionary<Node<Tile>, float>();

        OpenSet.Enqueue(start, 0);

        foreach (Node<Tile> n in nodes.Values)
        {
            g_score[n] = Mathf.Infinity;
        }
        g_score[start] = 0;

        foreach (Node<Tile> n in nodes.Values)
        {
            f_score[n] = Mathf.Infinity;
        }
        f_score[start] = HeuristicCostEstimate(start, goal);

        while (OpenSet.Count > 0)
        {
            Node<Tile> current = OpenSet.Dequeue();

            if (current == goal)
            {
                ConstructPath(Came_From, current);
                return;
            }

            ClosedSet.Add(current);

            foreach (Edge<Tile> edge_neighbor in current.edges)
            {
                Node<Tile> neighbor = edge_neighbor.node;

                if (ClosedSet.Contains(neighbor) == true)
                    continue;

                float movement_cost_to_neighbor = neighbor.tile.MovementCost * DistanceBetween(current, neighbor);

                float tentative_g_score = g_score[current] + movement_cost_to_neighbor;

                if (OpenSet.Contains(neighbor) && tentative_g_score >= g_score[neighbor])
                    continue;

                Came_From[neighbor] = current;
                g_score[neighbor] = tentative_g_score;
                f_score[neighbor] = g_score[neighbor] + HeuristicCostEstimate(neighbor, goal);

                if (OpenSet.Contains(neighbor) == false)
                {
                    OpenSet.Enqueue(neighbor, f_score[neighbor]);
                }
            }
        }
    }

    float HeuristicCostEstimate(Node<Tile> a, Node<Tile> b)
    {
        return Mathf.Sqrt(
            Mathf.Pow(a.tile.X - b.tile.X, 2) +
            Mathf.Pow(a.tile.Y - b.tile.Y, 2)
        );
    }

    float DistanceBetween(Node<Tile> a, Node<Tile> b)
    {
        if (Mathf.Abs(a.tile.X - b.tile.X) + Mathf.Abs(a.tile.Y - b.tile.Y) == 1)
        {
            return 1f;
        }

        return Mathf.Sqrt(
            Mathf.Pow(a.tile.X - b.tile.X, 2) +
            Mathf.Pow(a.tile.Y - b.tile.Y, 2)
        );

    }

    void ConstructPath(Dictionary<Node<Tile>, Node<Tile>> Came_From, Node<Tile> current)
    {
        Queue<Tile> total_path = new Queue<Tile>();
        total_path.Enqueue(current.tile);

        while (Came_From.ContainsKey(current))
        {
            current = Came_From[current];
            total_path.Enqueue(current.tile);
        }

        path = new Queue<Tile>(total_path.Reverse());
    }

    public Tile Dequeue()
    {
        return path.Dequeue();
    }

    public int Length()
    {
        return path != null ? path.Count : 0;
    }
}
