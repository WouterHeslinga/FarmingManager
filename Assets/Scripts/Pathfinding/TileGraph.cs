﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileGraph
{
    public Dictionary<Tile, Node<Tile>> Nodes { get; private set; }

    public TileGraph(World world)
    {
        Nodes = new Dictionary<Tile, Node<Tile>>();

        for (int x = 0; x < world.Width; x++)
        {
            for (int y = 0; y < world.Height; y++)
            {

                Tile t = world.GetTileAt(x, y);

                if(t.MovementCost > 0) {
                    Node<Tile> n = new Node<Tile>
                    {
                        tile = t
                    };
                    Nodes.Add(t, n);
                }

            }
        }

        foreach (Tile t in Nodes.Keys)
        {
            Node<Tile> n = Nodes[t];

            List<Edge<Tile>> edges = new List<Edge<Tile>>();

            Tile[] neighbours = t.GetNeighbours(false);

            for (int i = 0; i < neighbours.Length; i++)
            {
                if (neighbours[i] != null && neighbours[i].MovementCost > 0)
                {
                    Edge<Tile> e = new Edge<Tile>
                    {
                        cost = neighbours[i].MovementCost,
                        node = Nodes[neighbours[i]]
                    };

                    edges.Add(e);
                }
            }
            n.edges = edges.ToArray();
        }
    }
}
